module sitelenkijetesantakalu

go 1.14

require (
	github.com/alecthomas/repr v0.0.0-20200325044227-4184120f674c
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gosimple/slug v1.9.0 // indirect
	github.com/jinzhu/gorm v1.9.12 // indirect
	github.com/jinzhu/now v1.1.1 // indirect
	github.com/labstack/echo/v4 v4.1.16
	github.com/microcosm-cc/bluemonday v1.0.2 // indirect
	github.com/qor/assetfs v0.0.0-20170713023933-ff57fdc13a14 // indirect
	github.com/qor/qor v0.0.0-20200224122013-457d2e3f50e1 // indirect
	github.com/qor/render v1.1.1
	github.com/urfave/cli/v2 v2.2.0
)
