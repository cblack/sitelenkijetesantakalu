package app

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strings"
)

type OutputDirectory struct {
	Name            string `json:"name"`
	OutputDirectory string `json:"outputdir"`
}

type Subgroup struct {
	Name        string `json:"name"`
	Order       int    `json:"order"`
	Description string `json:"description"`
}

type Group struct {
	ID          string         `json:"name"`
	Name        string         `json:"fancyname"`
	Maintainer  MaintainerList `json:"maintainer"`
	MailingList string         `json:"mailinglist"`
	Platforms   PlatformList   `json:"platforms"`
	Summary     string         `json:"description"`
	Description []string       `json:"long_description"`
	Subgroups   []Subgroup
}

type Library struct {
	Maintainer MaintainerList `json:"maintainer"`
	Summary    string         `json:"description"`
	ID         string         `json:"name"`
	Name       string         `json:"fancyname"`
	Tier       int            `json:"tier"`
	Type       string         `json:"type"`
	Deprecated bool           `json:"deprecated"`
	Group      string         `json:"group"`
	SubGroup   string         `json:"subgroup"`
	Platforms  PlatformList   `json:"platforms"`
	Path       string
}

type MaintainerList []string
type PlatformList []string

func (m *MaintainerList) UnmarshalJSON(data []byte) error {
	var v interface{}
	err := json.Unmarshal(data, &v)
	if err != nil {
		return err
	}
	switch v.(type) {
	case string:
		*m = append(*m, v.(string))
	case []string:
		*m = append(*m, v.([]string)...)
	case []interface{}:
		for _, item := range v.([]interface{}) {
			switch item.(type) {
			case string:
				*m = append(*m, item.(string))
			}
		}
	}
	return nil
}

func (p *PlatformList) UnmarshalJSON(data []byte) error {
	var v interface{}
	err := json.Unmarshal(data, &v)
	if err != nil {
		return err
	}
	switch v.(type) {
	case []interface{}:
		for _, intf := range v.([]interface{}) {
			switch intf.(type) {
			case map[string]interface{}:
				*p = append(*p, intf.(map[string]interface{})["name"].(string))
			case string:
				*p = append(*p, intf.(string))
			}
		}
	}
	return nil
}

type Subcollection struct {
	Subgroup  Subgroup
	Libraries []Library
}

type Collection struct {
	Group     Group
	Subgroups map[string]Subcollection
	Libraries []Library
}

var Collections map[string]Collection

// ParseData : Parse data from data/.
func ParseData() {
	var outputDirectories []OutputDirectory
	var libraries []Library
	var groups []Group
	logger := log.New(os.Stderr, "ParseData:", log.Ldate|log.Ltime|log.Lshortfile)
	collections := map[string]Collection{
		"other": {
			Group: Group{
				ID:   "other",
				Name: "Other Libraries",
			},
		},
	}
	directories, err := ioutil.ReadDir("data")
	if err != nil {
		logger.Printf("Error reading data, cancelling refresh: %+v\n", err)
		return
	}
	for _, directory := range directories {
		if directory.IsDir() {
			// parse outputs.json
			outputData, err := ioutil.ReadFile(path.Join("data", directory.Name(), "outputs.json"))
			if err != nil {
				logger.Printf("Error reading data, skipping %s: %+v\n", directory.Name(), err)
				continue
			}
			var dirs []OutputDirectory
			err = json.Unmarshal(outputData, &dirs)
			if err != nil {
				logger.Printf("Error parsing JSON, skipping %s: %+v\n", directory.Name(), err)
				continue
			}
			var trueDirs []OutputDirectory
			for _, dir := range dirs {
				dir.OutputDirectory = path.Join(directory.Name(), dir.OutputDirectory)
				trueDirs = append(trueDirs, dir)
			}
			// parse metadata.json
			metadataData, err := ioutil.ReadFile(path.Join("data", directory.Name(), "metadata.json"))
			if err != nil {
				logger.Printf("Error reading data, skipping %s: %+v\n", directory.Name(), err)
				continue
			}
			var mds []struct {
				Library
				GroupInfo *Group `json:"group_info"`
			}
			err = json.Unmarshal(metadataData, &mds)
			if err != nil {
				logger.Printf("Error parsing JSON, skipping %s: %+v\n", directory.Name(), err)
				continue
			}
			outputDirectories = append(outputDirectories, trueDirs...)
			for _, metadata := range mds {
				if metadata.GroupInfo != nil {
					groups = append(groups, *metadata.GroupInfo)
				}
				libraries = append(libraries, metadata.Library)
			}
		}
	}
	for _, group := range groups {
		collections[group.ID] = Collection{
			Group:     group,
			Subgroups: make(map[string]Subcollection),
		}
		for _, subgroup := range group.Subgroups {
			collections[group.ID].Subgroups[subgroup.Name] = Subcollection{
				Subgroup: subgroup,
			}
		}
	}
	for _, library := range libraries {
	check:
		for _, out := range outputDirectories {
			if out.Name == library.ID {
				library.Path = out.OutputDirectory
				break check
			}
		}
		if group, ok := collections[strings.ToLower(library.Group)]; ok {
			if subgroup, ok := group.Subgroups[library.SubGroup]; ok {
				// if this library belongs to a subgroup, put it there
				subgroup.Libraries = append(subgroup.Libraries, library)
				group.Subgroups[library.SubGroup] = subgroup
			} else {
				// otherwise we put it in the group directly
				group.Libraries = append(group.Libraries, library)
			}
			// update the map
			collections[strings.ToLower(library.Group)] = group
		} else {
			grp := collections["other"]
			grp.Libraries = append(grp.Libraries, library)
			collections["other"] = grp
		}
	}
	Collections = collections
}
