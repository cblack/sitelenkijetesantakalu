package app

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/qor/render"
)

var renderer = render.New(&render.Config{
	ViewPaths:     []string{"app/templates"},
	DefaultLayout: "application",
	FuncMapMaker: func(*render.Render, *http.Request, http.ResponseWriter) template.FuncMap {
		return map[string]interface{}{
			"unescape": func(in string) template.HTML {
				return template.HTML(in)
			},
			"platform_chips": func(platforms []string) template.HTML {
				var chips []string
				for _, platform := range platforms {
					lower := strings.ToLower(platform)
					partial := strings.Contains(lower, "partial")
					tmpl := func(in string) string {
						if partial {
							return fmt.Sprintf(`<span class="badge badge-secondary">%s</span>`, in)
						}
						return fmt.Sprintf(`<span class="badge badge-primary">%s</span>`, in)
					}
					c := func(s1, s2 string) bool {
						return strings.Contains(s1, s2)
					}
					add := func(name, disp string) {
						if c(lower, name) {
							chips = append(chips, tmpl(disp))
						}
					}
					add("windows", "Windows")
					add("macos", "macOS")
					add("bsd", "FreeBSD")
					add("android", "Android")
					add("linux", "Linux")
				}
				var sb strings.Builder
				for _, platform := range chips {
					sb.WriteString(platform)
					sb.WriteString(" ")
				}
				return template.HTML(fmt.Sprintf(`<div>%s</div>`, sb.String()))
			},
			"join_strings": strings.Join,
		}
	},
})

type Template struct{}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	d := DummyByte{}
	err := renderer.Execute(name, data, &http.Request{}, &d)
	if err != nil {
		return err
	}
	_, err = w.Write([]byte(d.String()))
	return err
}
