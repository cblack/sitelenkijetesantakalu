package app

import "strings"

func InsensitiveCompare(str1, str2 string) bool {
	return strings.ToLower(str1) == strings.ToLower(str2)
}
