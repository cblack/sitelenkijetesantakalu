{{ define "maintainer" }}
    {{ if . }}
        <small class="text-muted">maintained by {{ join_strings . ", " }}</small>
    {{ end }}
{{ end }}

{{ define "library" }}
<div class="card">
    <div class="card-header">
        {{ .Name }} {{ template "maintainer" .Maintainer }} <br>
        {{ platform_chips .Platforms }}
    </div>
    <div class="card-body">
        <p class="card-text">{{ .Summary }}</p>
    </div>
    <div class="card-footer">
        <a href="/data/{{ .Path }}/html/index.html" class="btn btn-primary">View Documentation</a>
    </div>
</div>
{{ end }}

<header class="header">
    <nav class="navbar">
        <ol class="breadcrumb navbar-nav">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <li class="breadcrumb-link">
                <a href="#">KDE API Reference</a>
            </li>
        </ol>
    </nav>
</header>

<aside class="sidebar">
    <div id="sidebar-header" class="menu-box">
        <div class="menu-title">
            <h2><a href="https://api.kde.org">api.kde.org</a></h2>
        </div>
    </div>
    <div class="menu-box">
        <div class="menu-content">
            <p>If you are looking for documentation about KDE software or how to use Plasma, please refer to our <a
                    href="https://userbase.kde.org">userbase website</a>.</p>
        </div>
    </div>
</aside>

<article class="content" data-spy="scroll" data-target="#sidebar-list">

{{ range $id, $collection := . }}
    <h1 id="{{ $collection.Group.ID }}">{{ $collection.Group.Name }} {{ template "maintainer" $collection.Group.Maintainer }}</h1>
    {{ platform_chips $collection.Group.Platforms }}
    {{ if $collection.Group.Description }}
    <p class="lead">
        {{ range $text := $collection.Group.Description }}
            {{ $text | unescape }}
        {{ end }}
    </p>
    {{ end }}
    {{ range $id, $subcollection := $collection.Subgroups }}
        <h2 id="{{ $collection.Group.ID }}-{{ $subcollection.Subgroup.Order }}">{{ $subcollection.Subgroup.Name }}</h2>
        {{ if $subcollection.Subgroup.Description }}
        <p>
            {{ $subcollection.Subgroup.Description }}
        </p>
        {{ end }}
        {{ if $subcollection.Libraries }}
            <div class="card-deck">
                {{ range $library := $subcollection.Libraries }}
                    {{ template "library" $library }}
                {{ end }}
            </div>
        {{ end }}
    {{ end }}
    {{ if $collection.Libraries }}
        <div class="card-deck">
            {{ range $library := $collection.Libraries }}
                {{ template "library" $library }}
            {{ end }}
        </div>
    {{ end }}
    </div>
{{ end }}

</article>