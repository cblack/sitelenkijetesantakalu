package app

import (
	"fmt"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/urfave/cli/v2"
)

func Index(c echo.Context) error {
	return c.Render(http.StatusOK, "index", Collections)
}

var redirects = map[string]string{}

func Map(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if c.Request().URL != nil {
			path := strings.TrimSuffix(c.Request().URL.Path, "/")
			if val, ok := redirects[path]; ok {
				return c.Redirect(http.StatusMovedPermanently, "/"+val)
			}
		}
		return next(c)
	}
}

func Host(c *cli.Context) error {
	app := echo.New()
	app.Renderer = &Template{}

	watcher, err := fsnotify.NewWatcher()
	defer watcher.Close()
	if err != nil {
		panic(err)
	}
	if err := watcher.Add("data"); err != nil {
		panic(err)
	}

	go func() {
		timer := time.NewTimer(1 * time.Second)
		for {
			select {
			case <-watcher.Events:
				timer.Reset(1 * time.Second)
			case <-timer.C:
				time.Sleep(1 * time.Second)
				ParseData()
			}
		}
	}()

	app.GET("/", Index)
	app.Use(middleware.Recover())
	app.Static("/data", "data")
	if c.NArg() > 0 {
		app.Pre(middleware.HTTPSNonWWWRedirect())
	}
	app.Pre(Map)
	filepath.Walk("data", func(fpath string, info os.FileInfo, err error) error {
		if info.IsDir() {
			if _, err := os.Stat(path.Join(fpath, "index.html")); !os.IsNotExist(err) {
				dirs := strings.Split(fpath, "/")
				if len(dirs) > 3 {
					redirects[fmt.Sprintf("/%s", strings.Join(dirs[2:][:len(dirs[2:])-1], "/"))] = fpath
				}
			}
		}
		return nil
	})
	app.Start(":8000")
	return nil
}

// Run : start the web application.
func Run() {
	ParseData()
	cliapp := &cli.App{
		Commands: []*cli.Command{
			{
				Name:   "host",
				Action: Host,
			},
			{
				Name: "generate",
				Action: func(c *cli.Context) error {
					renderer.Execute("index", Collections, &http.Request{}, &DummyFile{})
					return nil
				},
			},
		},
	}
	cliapp.Run(os.Args)
}
